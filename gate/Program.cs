﻿using System;
using System.IO;
using System.Threading.Tasks;
using GrapeCity.ActiveReports;
using GrapeCity.ActiveReports.Drawing.Core;
using GrapeCity.ActiveReports.Export.Pdf.Page;
using GrapeCity.ActiveReports.Rdl.Persistence;
using GrapeCity.ActiveReports.Rendering;

namespace ConsoleApp5
{
	public static class Program
	{
		static void Main(string[] args)
		{
			// var report = new ReportStore(new Report(), new DefaultResourceLocator());
			// var iReport = report.BuildReport();
			// var export = new PdfExport(new FontsFactory());
			// using(var f = File.OpenWrite("C:\\!\\report.pdf"))
			// 	export.Export(iReport, f);
			RenderToPdf("C:\\!\\11111.rdlx", "c:\\!\\11111.pdf");
		}

		public static bool RenderToPdf (string filePath, string pdfFilePath)
		{
			var resourceLocator = new DefaultResourceLocator {BaseUri = new Uri(filePath)};

			using (var stream = File.OpenText(filePath))
			using (var f = File.OpenWrite(pdfFilePath))
			using (var template = PersistenceFilter.Load(stream, resourceLocator))
			{
				var report = new ReportStore(template, resourceLocator);
				var iReport = report.BuildReport();
				var export = new PdfExport(new FontsFactory());
				export.Export(iReport, f);
				return true;
			}
		}

		public static async Task<object> Invoke(dynamic input)
		{
			var filePath = (string)input.filePath;
			var pdfFilePath = (string)input.pdfFilePath;

			return await Task.FromResult(RenderToPdf(filePath, pdfFilePath));
		}
	}
}