var edge = require('edge-js');
var path = require('path');

var helloWorld = edge.func(function () {/*
    async (input) => { 
        return ".NET Welcomes " + input.ToString(); 
    }
*/});
helloWorld('JavaScript', function (error, result) {
    if (error) throw error;
    console.log(result);
});

if(0) {
    var render = edge.func({
        assemblyFile: '..\\gate\\bin\\Release\\netcoreapp2.1\\win-x64\\ConsoleApp5.dll',
        typeName: 'ConsoleApp5.Program',
        methodName: 'Invoke' // This must be Func<object,Task<object>>
    });
    render({ filePath: "C:\\!\\11111.rdlx", pdfFilePath: "c:\\!\\222-222.pdf"},
        function (error, result) {
            if (error) throw error;
            console.log(result);
    });

} else {
    var render1 = edge.func({
        source: function() {/*
        #r "..\\gate\\bin\\Release\\netcoreapp2.1\\win-x64\\publish\\ConsoleApp5.dll"
        using System.Threading.Tasks;

        public class Startup
        {
            public Task<object> Invoke(object input)
            {
                return ConsoleApp5.Program.Invoke(input);
            }
        }
        */},
        //references: [path.join(__dirname, '..\\gate\\bin\\Release\\netcoreapp2.1\\win-x64\\ConsoleApp5.dll')]
        });

    // clrMethod("C:\\!\\11111.rdlx", "c:\\!\\222-222.pdf")
    helloWorld('JavaScript', function (error, result) {
        if (error) throw error;
        console.log(result);
    });

    render1({ filePath: "C:\\!\\11111.rdlx", pdfFilePath: "c:\\!\\222-222.pdf"},
        function (error, result) {
            if (error) throw error;
            console.log(result);
        });
    }